package com.vtb;

import org.apache.poi.xwpf.converter.pdf.PdfConverter;
import org.apache.poi.xwpf.converter.pdf.PdfOptions;
import org.apache.poi.xwpf.usermodel.XWPFDocument;

import java.io.*;

public class WordConverter {
    /**
     * Конвертит docx в pdf
     * @param docPath путь до дока
     * @param pdfPath путь до итогового файла
     * @throws IOException
     */
    public static void convertToPDF(String docPath, String pdfPath) throws IOException {
            InputStream doc = new FileInputStream(new File(docPath));
            convertToPDF(doc,pdfPath);
    }

    /**
     * Конвертит docx в pdf
     * @param doc InputStream дока
     * @param pdfPath путь до итогового файла
     * @throws IOException
     */
    public static void convertToPDF(InputStream doc, String pdfPath) throws IOException {
        XWPFDocument document = new XWPFDocument(doc);
        PdfOptions options = PdfOptions.create();
        OutputStream out = new FileOutputStream(new File(pdfPath));
        PdfConverter.getInstance().convert(document, out, options);
    }

}

