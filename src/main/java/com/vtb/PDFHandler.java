package com.vtb;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfReader;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.element.Image;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.text.html.StyleSheet;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.*;

public class PDFHandler {
    /**
     *
     * @param imFilebytes byte array of image
     * @param width width
     * @param height width
     * @param topOffset width
     * @param leftOffset leftOffset
     * @param source path to source pdf
     * @param target path to target pdf
     * @throws IOException
     */
    public static void addImageToPDF(byte[] imFilebytes,int width, int height, int topOffset, int leftOffset, String source, String target) throws IOException {
        // Modify PDF located at "source" and save to "target"
        PdfReader reader = new PdfReader(source);
        addImageToPdf(imFilebytes, width, height, topOffset, leftOffset, target, reader);
    }
    /**
     *
     * @param imFilebytes byte array of image
     * @param width width
     * @param height width
     * @param topOffset width
     * @param leftOffset leftOffset
     * @param source InputStream of source pdf
     * @param target path to target pdf
     * @throws IOException
     */
    public static void addImageToPDF(byte[] imFilebytes,int width, int height, int topOffset, int leftOffset, InputStream source, String target) throws IOException {
        // Modify PDF located at "source" and save to "target"
        PdfReader reader = new PdfReader(source);
        addImageToPdf(imFilebytes, width, height, topOffset, leftOffset, target, reader);
    }

    /**
     *
     * @param imFilebytes byte array of image
     * @param width width
     * @param height width
     * @param topOffset width
     * @param leftOffset leftOffset
     * @param target path to target pdf
     * @param reader PdfReader
     * @throws FileNotFoundException
     */
    private static void addImageToPdf(byte[] imFilebytes, int width, int height, int topOffset, int leftOffset, String target, PdfReader reader) throws FileNotFoundException {
        PdfDocument pdfDocument = new PdfDocument(reader, new PdfWriter(target));
        // Document to add layout elements: paragraphs, images etc
        Document document = new Document(pdfDocument);

        // Load image from disk
        ImageData imageData = ImageDataFactory.create(imFilebytes);
        // Create layout image object and provide parameters. Page number = 1
        Float heightPage = document.getPdfDocument().getPage(1).getPageSize().getHeight();
        Image image = new Image(imageData).scaleAbsolute(width, height).setFixedPosition(1, leftOffset, heightPage-height-topOffset);
        // This adds the image to the page
        document.add(image);
        // Don't forget to close the document.
        // When you use Document, you should close it rather than PdfDocument instance
        document.close();
    }

    /**
     *
     * @param html html string
     * @param width width
     * @param height width
     * @param topOffset width
     * @param leftOffset leftOffset
     * @param source path to source pdf
     * @param target path to target pdf
     * @throws IOException
     */
    public static void addHtmlToPDF(String html,int width, int height, int fontSize, int topOffset, int leftOffset, String source, String target) throws IOException {
        byte[] bytes = htmlToImageBytes(html, width, height, fontSize);
        addImageToPDF(bytes,width,height,topOffset,leftOffset,source,target);
    }
    /**
     *
     * @param html html string
     * @param width width
     * @param height width
     * @param topOffset width
     * @param leftOffset leftOffset
     * @param source InputStream of source pdf
     * @param target path to target pdf
     * @throws IOException
     */
    public static void addHtmlToPDF(String html,int width, int height, int fontSize, int topOffset, int leftOffset, InputStream source, String target) throws IOException {
        byte[] bytes = htmlToImageBytes(html, width, height, fontSize);
        addImageToPDF(bytes,width,height,topOffset,leftOffset,source,target);
    }

    /**
     * Конвертит html в картинку
     * @param html html string
     * @param width  width
     * @param height height
     * @param fontSize font Size
     * @return байт array картинки
     * @throws IOException
     */
    private static byte[] htmlToImageBytes(String html, int width, int height, int fontSize) throws IOException {
        JLabel label = new JLabel(html);
        label.setSize(width, height);
        label.setVerticalAlignment(SwingConstants.TOP);
        Font font = label.getFont();
        label.setFont(new Font(font.getName(),Font.PLAIN,fontSize));
        BufferedImage image = new BufferedImage(
                label.getWidth(), label.getHeight(),
                BufferedImage.TYPE_INT_RGB);

        // paint the html to an image
        Graphics g = image.getGraphics();
        g.setColor(Color.WHITE);
        g.fillRect(0, 0, image.getWidth(), image.getHeight());
        g.setColor(Color.BLACK);
        label.paint(g);
        g.dispose();


        // get the byte array of the image (as jpeg)
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        ImageIO.write(image, "jpg", baos);
        return baos.toByteArray();
    }
}
